FROM dc2.srvhub.tools/shb/postgres:10
MAINTAINER Sergey Bespalov
ARG POSTGRE_HOST
ARG POSTGRE_PORT
ARG POSTGRE_USER
ARG POSTGRE_SCHEMA
ARG POSTGRE_PASSWORD

# Copy from https://habr.com/post/328226/

# Do NOT use /var/lib/postgresql/data/ because its declared as volume in base image and can't be undeclared but we want persist data in image
ENV PGDATA /var/lib/pgsql/data/
ENV pgsql "psql -U $POSTGRE_USER -nxq -v ON_ERROR_STOP=on --dbname $POSTGRE_SCHEMA"

COPY docker-entrypoint-initdb.d/* /docker-entrypoint-initdb.d/

RUN ln -sb /bin/bash /bin/sh

RUN set -euo pipefail \
    && sed -i -e "s/USER2REPLACE/$POSTGRE_USER/g" -e "s/SCHEMA2REPLACE/$POSTGRE_SCHEMA/g" docker-entrypoint-initdb.d/init-user-db.sh\
    && cat docker-entrypoint-initdb.d/init-user-db.sh \
    && echo '1) Install required packages' `# https://docs.docker.com/engine/userguide/eng-image/dockerfile_best-practices/#apt-get` \
#        && apt-get update \
#        && apt-get install -y \
#            curl \
    && echo '2) Run postgres DB internally for init cluster:' `# Example how to run instance of service: http://stackoverflow.com/questions/25920029/setting-up-mysql-and-importing-dump-within-dockerfile`\
        && bash -c '/docker-entrypoint.sh postgres --autovacuum=off &' \
            && sleep 15 \
    && echo '3) Configure listen_addresses = * in postgresql.conf :' \
    && sed -i 's/#listen_addresses = \x27localhost\x27/listen_addresses = \x27*\x27/g' $PGDATA/postgresql.conf \
    && echo '4.1) Configure postgres: Do NOT chown and chmod each time on start PGDATA directory (speedup on start especially on Windows):' \
        && sed -i 's@chmod 700 "$PGDATA"@#chmod 700 "$PGDATA"@g;s@chown -R postgres "$PGDATA"@#chown -R postgres "$PGDATA"@g' /docker-entrypoint.sh \
    && echo '4.2) stop server:'\
        && gosu postgres pg_ctl -D "$PGDATA" -m fast -w stop \
            && sleep 10 \
    && echo '4.3) RERun postgres DB for work in new configuration:'\
        && bash -c '/docker-entrypoint.sh postgres --autovacuum=off --max_wal_size=3GB &' \
            && sleep 10 \
    && echo '5) Populate DB data: Restore DB backup:' \
     && PGPASSWORD="$POSTGRE_PASSWORD" pg_dump -C -h $POSTGRE_HOST -p $POSTGRE_PORT -U $POSTGRE_USER $POSTGRE_SCHEMA \
                | grep -Pv '^((DROP|CREATE|ALTER) DATABASE|\\connect)' \
                    | $pgsql \
    && echo '6) Execute build-time sql scripts:' \
        && for f in /init.sql/*; do echo "Process [$f]"; $pgsql -f "$f"; rm -f "$f"; done \
#    && echo '7) Update DB to current migrations state:' \
#        && time java -jar target/egais-db-updater-*.jar -f flyway.url=jdbc:postgresql://localhost:5432/egais -f flyway.user=postgres -f flyway.password=postgres \
    && echo '8) Vacuum full and analyze (no reindex need then):' \
        && time vacuumdb -U postgres --full --all --analyze --freeze \
    && echo '9) Stop postgres:' \
    && gosu postgres pg_ctl -D "$PGDATA" -m fast -w stop \
        && sleep 10 \
    && echo '10) Cleanup pg_xlog required to do not include it in image!:' \
        && gosu postgres pg_resetwal -o $( LANG=C pg_controldata $PGDATA | grep -oP '(?<=NextOID:\s{10})\d+' ) -x $( LANG=C pg_controldata $PGDATA | grep -oP '(?<=NextXID:\s{10}0[/:])\d+' ) -f $PGDATA \
    && echo '11(pair to 1)) Apt clean:' \
#        && apt-get autoremove -y \
#            curl \
#        && rm -rf /var/lib/apt/lists/*
